﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    delegate Fraction Operator(Fraction numerator, Fraction denominator);

    public class Fraction
    {
        private readonly int num;
        private readonly int den;

        public Fraction(int numerator, int denominator)
        {
            if (denominator == 0)
            {
                throw new ArgumentException("Denominator cannot be zero.", nameof(denominator));
            }

            num = numerator;
            den = denominator;
        }

        public static Fraction Add(Fraction a) => a;
        public static Fraction Substract(Fraction a) => new Fraction(-a.num, a.den);

        public static Fraction Add(Fraction a, Fraction b)
            => new Fraction(a.num * b.den + b.num * a.den, a.den * b.den);

        public static Fraction Substract(Fraction a, Fraction b)
            => Add(a, Substract(b));

        public static Fraction Multiply(Fraction a, Fraction b)
            => new Fraction(a.num * b.num, a.den * b.den);

        public static Fraction Divide(Fraction a, Fraction b)
        {
            if (b.num == 0)
            {
                throw new DivideByZeroException();
            }
            return new Fraction(a.num * b.den, a.den * b.num);
        }

        static Dictionary<char, Operator> operators = new Dictionary<char, Operator>()
        {
            { '+', Add },
            { '-', Substract },
            { '*', Multiply },
            { '/', Divide }
        };

        public override string ToString()
        {
            int gcd;
            if (Math.Abs(num) / den == 0)
            {
                gcd = FindGcd(num, den);
                return $"{num / gcd}/{den / gcd}";
            }
            else
            {
                gcd = FindGcd(Math.Abs(num) % den, den);
                return $"{num / den} {(Math.Abs(num) % den) / gcd}/{den / gcd}";
            }
        }

        static int FindGcd(int num, int den)
        {
            while (num != den)
            {
                if (num > den)
                    num = num - den;
                else
                    den = den - num;
            }
            return num;
        }

        public static void Execute()
        {
            var data = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            List<Fraction> fractions = new List<Fraction>();
            int num = 0, cel = 0;
            Operator operation = null;
            for (int i = 0; i < data.Length; i++)
            {
                if(!int.TryParse(data[i], out num))
                {
                    var conv = data[i].Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if(conv.Length == 2)
                    {
                        cel = cel * int.Parse(conv[1]) + int.Parse(conv[0]);
                        fractions.Add(new Fraction(cel, int.Parse(conv[1])));
                        cel = 0;
                    }
                    else
                    {
                        operation = operators[conv[0][0]];
                    }
                }
                else
                {
                    cel = num;
                }
            }

            var result = operation(fractions[0], fractions[1]);
            Console.WriteLine("Result: " + result);
        }
    }
}
