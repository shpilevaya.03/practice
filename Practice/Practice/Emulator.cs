﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Emulator
    {
        const string exit =        "exit";
        const string cd =          "cd";
        const string newFile =     "new";
        const string deleteFile =  "delete";
        const string editFile =    "edit";

        public static void Execute()
        {
            var main = Directory.GetCurrentDirectory();
            DirectoryInfo actualDirectory = new DirectoryInfo(main);
            Console.WriteLine("Actual directory: " + main + "\n\n" + "Files:", Console.ForegroundColor = ConsoleColor.DarkGreen);
            WriteAllFiles(actualDirectory);

            Console.WriteLine("\nWrite command:");

            var data = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            while (!(data.Length == 1 && data[0] == exit))
            {
                if(data.Length != 2)
                {
                    Console.WriteLine("This command is not found. Try again.\n");
                    data = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    continue;
                }

                var command =   data[0];
                var fileName =  data[1];
                switch(data[0])
                {
                    case cd:
                        var directories = actualDirectory.GetDirectories();
                        if (data[1] == "..") //вернуться на каталог выше
                            actualDirectory = actualDirectory.Parent;
                        else if (!directories.All(dir => dir.Name != fileName)) //войти в существующий каталог
                            actualDirectory = directories.First(dir => dir.Name == fileName);
                        WriteAllFiles(actualDirectory);
                        break;

                    case newFile:
                        if (actualDirectory.GetFiles().All(file => file.Name != fileName))
                            File.Create(actualDirectory.FullName + "\\" + fileName);
                        else
                            Console.WriteLine("File with that name already exists.");
                        Console.WriteLine();
                        break;

                    case deleteFile:
                        if (!actualDirectory.GetFiles().All(file => file.Name != fileName))
                            File.Delete(actualDirectory.FullName + "\\" + fileName);
                        else
                            Console.WriteLine("File is not found.");
                        Console.WriteLine();
                        break;

                    case editFile:
                        if (!actualDirectory.GetFiles().All(file => file.Name != fileName))
                        {
                            Console.WriteLine("Write text: ");
                            var text = Console.ReadLine();
                            File.WriteAllText(actualDirectory.FullName + "\\" + fileName, text);
                        }
                        else
                            Console.WriteLine("File is not found.");
                        Console.WriteLine();
                        break;

                    default:
                        Console.WriteLine("This command is not found. Try again.\n");
                        break;
                }
                data = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        static void WriteAllFiles(DirectoryInfo directoryInfo)
        {
            Array.ForEach(directoryInfo.GetDirectories(), file => Console.WriteLine(file.FullName));
            Array.ForEach(directoryInfo.GetFiles(), file => Console.WriteLine(file.FullName));

            Console.WriteLine();
        }
    }
}
