﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Practice
{
    public class Exchange
    {
        public string Name { get; set; }
        public List<Currency> Currencies = new List<Currency>();
    }

    public class Currency
    {
        public string Name      { get; set; }
        public double Purchase  { get; set; }
        public double Sale      { get; set; }
    }

    public class PracticeSubtitles
    {
        public static int CurrencyNameIndex =       0;
        public static int CurrencyPurchaseIndex =   1;
        public static int CurrencySaleIndex =       2;

        public static int MainCurrencyIndex = 0;

        public static string Path = @"data.txt";

        static void Main()
        {
            var data = File.ReadAllText(Path);
            Exchange exchange = MakeNewExchange(data);
            List<Exchange> exchanges = new List<Exchange>();

            for (int i = 0; i < exchange.Currencies.Count; i++)
            {
                exchanges.Add(MakeNewExchange(exchange.Name, exchange.Currencies[i]));
            }

            WriteExchangeToConsole(exchange);

            Console.WriteLine(new String('_', 10));
            WriteExchangeToConsole(exchanges.ToArray());

            foreach (var curr in exchanges)
            {
                WriteExchangeToFile(curr);
            }
        }

        static Currency GetCurrency(string line)
        {
            string[] parts = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return new Currency
            {
                Name = parts[CurrencyNameIndex],
                Purchase = double.Parse(parts[CurrencyPurchaseIndex]),
                Sale = double.Parse(parts[CurrencySaleIndex])
            };
        }

        public static Exchange MakeNewExchange(string mainCurrency, Currency currency)
        {
            return new Exchange {
                Name = currency.Name,
                Currencies = new List<Currency>() { 
                    new Currency { Name = mainCurrency, Purchase = 1 / currency.Purchase, Sale = 1 / currency.Sale }
                }
            };
        }
        public static Exchange MakeNewExchange(string data)
        {
            Exchange exchange = new Exchange();
            var splitData = data.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            exchange.Name = splitData[MainCurrencyIndex];
            for (int i = MainCurrencyIndex + 1; i < splitData.Length; i++)
            {
                exchange.Currencies.Add(GetCurrency(splitData[i]));
            }

            return exchange;
        }

        public static void WriteExchangeToConsole(Exchange[] exchanges)
        {
            foreach (var curr in exchanges)
            {
                Console.WriteLine(curr.Name);
                foreach (var item in curr.Currencies)
                {
                    Console.WriteLine(item.Name + "\t" + item.Purchase + "\t" + item.Sale);
                }
                Console.WriteLine();
            }
        }

        public static void WriteExchangeToConsole(Exchange exchange)
        {
            Console.WriteLine(exchange.Name);
            foreach (var item in exchange.Currencies)
            {
                Console.WriteLine(item.Name + "\t" + item.Purchase + "\t" + item.Sale);
            }
            Console.WriteLine();
        }

        static void WriteExchangeToFile(Exchange exchange)
        {
            StringBuilder text = new StringBuilder();
            text.AppendLine(exchange.Name);

            foreach (var curr in exchange.Currencies)
            {
                text.AppendLine(curr.Name + " " + curr.Purchase + " " + curr.Sale);
            }

            File.WriteAllText(exchange.Name + ".txt", text.ToString());
        }

        static void GenerateData()
        {
            string text = @"RUB
USD 75,00 76,84
EUR 85,76 88,76
JPY 64,97 69,25
CNY 11,65 12,24
CHF 81,16 85,32";
            File.WriteAllText(Path, text);
        }
    }
}