﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Gallows
    {
        static string path =    Directory.GetCurrentDirectory() + "\\words.txt";
        static int attempts =   13;
        static int charsCount = 0;

        public static void Execute()
        {
            string[] words = File.ReadAllLines(path);
            List<char> usedChar = new List<char>();

            Random random = new Random();
            string hiddenWord = words[random.Next(words.Length)];
            charsCount = hiddenWord.Length;
            string visibleWord = new string('_', hiddenWord.Length);
            Console.WriteLine("Word: " + visibleWord + "\t\t" + "remaining number of attempts: " + attempts);

            while (attempts != 0 && charsCount != 0)
            {
                try
                {
                    char input = Console.ReadKey().KeyChar;

                    if(hiddenWord.Contains(input) && usedChar.All(symb => symb != input))
                    {
                        for (int i = 0; i < hiddenWord.Length; i++)
                        {
                            if (hiddenWord[i] == input)
                            {
                                visibleWord = visibleWord.Remove(i, 1).Insert(i, input.ToString());
                                charsCount--;
                            }
                        }
                    }
                    else if(usedChar.All(symb => symb != input))
                    {
                        attempts--;
                    }

                    ClearConsoleLine();
                    if(usedChar.All(symb => symb != input))
                        usedChar.Add(input);
                    Console.WriteLine("Word: " + visibleWord + "\t\t" + "remaining number of attempts: " + attempts);
                    Console.Write("Used symbols: ");
                    foreach (var symb in usedChar)
                        Console.Write(symb + " ");
                    Console.WriteLine();
                }

                catch(Exception e)
                {
                    Console.WriteLine("An error has occurred: " + e.Message);
                }
            }

            if (attempts == 0)
                Console.WriteLine("You're louser!\nThe hidden word: " + hiddenWord);
            else if (charsCount == 0)
                Console.WriteLine("You're winner!");
        }
        static void ClearConsoleLine()
        {
            for (int i = 0; i < 1; i++)
            {
                Console.MoveBufferArea(0, i, Console.BufferWidth, 1, Console.BufferWidth, 0, ' ', Console.ForegroundColor, Console.BackgroundColor);
            }
            Console.SetCursorPosition(0, 0);
        }

        static void GenerateData()
        {
            string data = @"Кант
Хроника
Зал
Галера
Балл
Вес
Кафель
Знак
Фильтр
Башня
Кондитер
Омар
Чан
Пламя
Банк
Тетерев
Муж
Камбала
Груз
Кино
Лаваш
Калач
Геолог
Бальзам
Бревно
Жердь
Борец
Самовар
Карабин
Подлокотник
Барак
Мотор
Шарж
Сустав
Амфитеатр
Скворечник
Подлодка
Затычка
Ресница
Спичка
Кабан
Муфта
Синоптик
Характер
Мафиози
Фундамент
Бумажник
Библиофил
Дрожжи
Казино
Конечность
Пробор
Дуст
Комбинация
Мешковина
Процессор
Крышка
Сфинкс
Пассатижи
Фунт
Кружево
Агитатор
Формуляр
Прокол
Абзац
Караван
Леденец
Кашпо
Баркас
Кардан
Вращение
Заливное
Метрдотель
Клавиатура
Радиатор
Сегмент
Обещание
Магнитофон
Кордебалет
Заварушка";
            File.WriteAllText(path, data.ToLower());
        }
    }
}
