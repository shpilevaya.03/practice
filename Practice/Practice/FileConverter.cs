﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Practice
{
    class FileConverter
    {
        static public void ConvertFile()
        {
            Console.WriteLine("Кодирование файла выполняется командой\n" +
                "1. FileEncoder encode --source buffer --file index.html // закодировать файл index.html и результат сохранить в буфер\n" +
                "2. FileEncoder encode -s file -f index.html // закодировать файл index.html и результат сохранить в файл index_html.txt\n\n" +
                "Декодирование файла выполняется командой\n" +
                "1. FileEncoder decode --source file --file index_html.txt // декодировать файл, base64 строка находится в файле indexhtml.txt, создать итоговый файл с именем index.html\n" +
                "2. FileEncoder decode -s buffer -f index.html // декодировать файл, base64 строка находится в буфере, создать итоговый файл с именем index.html\n\n--source\tисточник для хранения base64 строки\n--file\t\tимя файла, от которого отталкиваемся");
            Console.WriteLine("\nВведите команду:");
            string[] data = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Convertation(data);
            Console.WriteLine(Clipboard.GetText());
        }

        static void Convertation(string[] data)
        {
            while (data.Length != 4
                  || data[0] != "FileEncoder"
                  || (data[1] != "encode" && data[1] != "decode")
                  || (data[2] != "file" && data[2] != "buffer")
                  || !File.Exists(data[3]))
            {
                Console.WriteLine("Invalid input, try again");
                data = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            }

            var code = data[1];
            var source = data[2];
            var path = data[3];

            switch (code)
            {
                case "encode":
                    ExecuteEncode(path, source);
                    break;
                case "decode":
                    ExecuteDecode(path, source);
                    break;
            }
        }

        static void ExecuteEncode(string path, string source)
        {
            Byte[] bytes = File.ReadAllBytes(path); 
            String file = Convert.ToBase64String(bytes);

            switch (source)
            {
                case "file":
                    File.WriteAllText(string.Join("_", path.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries)) + ".txt", file);
                    break;
                case "buffer":
                    Clipboard.SetText(file);
                    break;
            }
        }

        static void ExecuteDecode(string path, string source)
        {
            Byte[] bytes = Convert.FromBase64String(File.ReadAllText(path));

            switch (source)
            {
                case "file":
                    string filePath = string.Join(".", path.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries));
                    File.WriteAllBytes(filePath.Remove(filePath.Length - 4), bytes);
                    break;
                case "buffer":
                    Clipboard.SetText(Encoding.Default.GetString(bytes.Where(x => x != 0).ToArray()));
                    break;
            }
        }
    }
}
