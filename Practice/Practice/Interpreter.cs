﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    enum Command
    {
        var, //объявление
        mov, //инициализация
        add, //добавить значение
        sub, //вычесть значение
        div, //деление
        mul  //умножение
    }

    class Argument
    {
        public static List<Argument> arguments = new List<Argument>();
        public string Key { get; set; }
        public double Value { get; set; }
        public Argument(string key, string value = "")
        {
            double val = 0;
            Key = key;
            if (value == "" || double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out val))
                Value = val;
            else if (!arguments.All(argument => argument.Key != value))
                Value = arguments.First(argument => argument.Key == value).Value;
        }

        public Argument(string key, double value = 0)
        {
            Key = key;
            Value = value;
        }
    }

    static class Operation
    {
        public static void Execute(Command command, Argument argument)
        {
            switch (command)
            {
                case Command.var:
                    ExecuteVar(argument);
                    break;
                case Command.mov:
                    ExecuteMov(argument);
                    break;
                case Command.add:
                    ExecuteAdd(argument);
                    break;
                case Command.sub:
                    ExecuteSub(argument);
                    break;
                case Command.mul:
                    ExecuteMul(argument);
                    break;
                case Command.div:
                    ExecuteDiv(argument);
                    break;
            }
        }

        static void ExecuteVar(Argument argument)
        {
            if (Argument.arguments.All(argmnt => argmnt.Key != argument.Key))
                Argument.arguments.Add(argument);
            else
                Console.WriteLine("Переменная с таким именем уже существует");
        }
        static void ExecuteMov(Argument argument)
        {
            if (Argument.arguments.All(argmnt => argmnt.Key != argument.Key))
                Console.WriteLine("Переменной с таким именем не существует - не удалось инициализировать переменную");
            else
                Argument.arguments.First(argmnt => argmnt.Key == argument.Key).Value = argument.Value;
        }
        static void ExecuteAdd(Argument argument)
        {
            if (Argument.arguments.All(argmnt => argmnt.Key != argument.Key))
                Console.WriteLine("Переменной с таким именем не существует - не удалось добавить значение");
            else
                Argument.arguments.First(argmnt => argmnt.Key == argument.Key).Value += argument.Value;
        }
        static void ExecuteSub(Argument argument)
        {
            if (Argument.arguments.All(argmnt => argmnt.Key != argument.Key))
                Console.WriteLine("Переменной с таким именем не существует - не удалось вычесть значение");
            else
                Argument.arguments.First(argmnt => argmnt.Key == argument.Key).Value -= argument.Value;
        }
        static void ExecuteMul(Argument argument)
        {
            if (Argument.arguments.All(argmnt => argmnt.Key != argument.Key))
                Console.WriteLine("Переменной с таким именем не существует - не удалось умножить на значение");
            else
                Argument.arguments.First(argmnt => argmnt.Key == argument.Key).Value *= argument.Value;
        }
        static void ExecuteDiv(Argument argument)
        {
            if (Argument.arguments.All(argmnt => argmnt.Key != argument.Key))
                Console.WriteLine("Переменной с таким именем не существует - не удалось поделить на значение");
            else
                Argument.arguments.First(argmnt => argmnt.Key == argument.Key).Value /= argument.Value;
        }

        public static void DoInterpreter(string path)
        {
            var str = File.ReadAllText(path).Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            Command command;
            for (int i = 0; i < str.Length; i++)
            {
                var data = str[i].Split(new[] { '\r', ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (Enum.IsDefined(typeof(Command), data[0]))
                    command = (Command)Enum.Parse(typeof(Command), data[0]);
                else
                    throw new Exception("Строка не является командой");

                var key = data[1];
                var value = data.Length > 2 ? data[2] : "";
                Execute(command, new Argument(key, value));
            }

            for (int i = 0; i < Argument.arguments.Count(); i++)
            {
                Console.WriteLine($"{Argument.arguments[i].Key} = {Argument.arguments[i].Value}");
            }
        }
    }
}
