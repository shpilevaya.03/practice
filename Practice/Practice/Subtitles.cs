﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Practice
{
    public class Subtitle
    {
        public DateTime BeginTime   { get; set; }
        public DateTime EndTime     { get; set; }
        public string Message       { get; set; }

        public Position position    { get; set; }
        public ConsoleColor color   { get; set; }

        private static DateTime _maxTime = DateTime.MinValue;
        public static DateTime MaxTime { 
            get
            {
                return _maxTime;
            }
            set 
            {
                if (value > _maxTime)
                    _maxTime = value;
            }
        }

        public enum Position
        {
            Bottom,
            Left,
            Top,
            Right
        }

        public Subtitle(DateTime beginTime, DateTime endTime, string message, Position position = Position.Bottom, ConsoleColor color = ConsoleColor.White)
        {
            this.BeginTime =    beginTime;
            this.EndTime =      endTime;
            this.Message =      message;
            this.position =     position;
            this.color =        color;
            MaxTime =           endTime;
        }

        public static Subtitle ConvertDataToSubtitles(string data)
        {
            var dataSplit = data.Split(new[] { ' ', '-', '[', ']', '\r', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            ConsoleColor color = ConsoleColor.White;
            Position pos = Position.Bottom;
            return new Subtitle(
                ConvertToDateTime(dataSplit[0]),
                ConvertToDateTime(dataSplit[1]),
                ConvertPosition(dataSplit[2], out pos) && ConvertColor(dataSplit[3], out color)
                    ? string.Join(" ", dataSplit.ToArray()).Remove(0, pos.ToString().Length + color.ToString().Length + dataSplit[0].Length * 2 + 4)
                    : string.Join(" ", dataSplit.ToArray()).Remove(0, dataSplit[0].Length * 2 + 2),
                pos,
                color
                );
        }

        static DateTime ConvertToDateTime(string data)
        {
            DateTime dateTime;
            if (!DateTime.TryParseExact(data, "mm:s", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                throw new Exception("Строка со временем имела неверный формат");
            return dateTime;
        }

        /// <summary>
        /// Возвращает true, если строка обозначает позицию и с помощью модификатора out - Position, иначе возвращает false
        /// </summary>
        /// <param name="data">Строка</param>
        /// <param name="position">Возвращаемое значение, если string это Position</param>
        /// <returns></returns>
        static bool ConvertPosition(string data, out Position position)
        {
            bool flag = false;
            position = new Position();

            if (Enum.IsDefined(typeof(Position), data))
            {
                position = (Position)Enum.Parse(typeof(Position), data);
                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Возвращает true, если строка обозначает цвет и с помощью модификатора out - ConsoleColor, иначе возвращает false
        /// </summary>
        /// <param name="data">Строка</param>
        /// <param name="color">Возвращаемое значение, если string это ConsoleColor</param>
        /// <returns></returns>
        static bool ConvertColor(string data, out ConsoleColor color)
        {
            bool flag = false;
            color = new ConsoleColor();

            if (Enum.IsDefined(typeof(ConsoleColor), data))
            {
                color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), data);
                flag = true;
            }

            return flag;
        }
    }

    class Subtitles
    {
        static FileInfo path = new FileInfo(@"subtitlesData.txt");
        const int width = 70;
        const int height = 25;

        static List<Subtitle> dataSplit = new List<Subtitle>();

        static public void DrawSubtitles()
        {
            string[] data = File.ReadAllText(path.FullName).Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < data.Length; i++)
            {
                dataSplit.Add(Subtitle.ConvertDataToSubtitles(data[i]));

                Subtitle subtitle = dataSplit[i];

                (int Left, int Top) cursor = (0, 0);

                switch (subtitle.position)
                {
                    case Subtitle.Position.Top:
                        cursor = ((width - subtitle.Message.Length) / 2, 1);
                        break;
                    case Subtitle.Position.Bottom:
                        cursor = ((width - subtitle.Message.Length) / 2, height + 1);
                        break;
                    case Subtitle.Position.Left:
                        cursor = (1, (height - subtitle.Message.Length) / 2);
                        break;
                    case Subtitle.Position.Right:
                        cursor = (width - subtitle.Message.Length + 1, (height - subtitle.Message.Length) / 2);
                        break;
                }
                Console.SetCursorPosition(cursor.Left, cursor.Top);

                Console.WriteLine(subtitle.Message, Console.ForegroundColor = subtitle.color);
                Thread.Sleep(subtitle.EndTime - subtitle.BeginTime);
                ClearConsoleLine(cursor.Top, cursor);
            }
        }

        /// <summary>
        /// Стирает строку в консоли с последующим перемещением курсора на ту строку
        /// </summary>
        /// <param name="line">номер строки</param>
        /// <param name="cursor">кортеж с положением курсора (номер столбца, номер строки)</param>
        static void ClearConsoleLine(int line, (int Left, int Top) cursor)
        {
            Console.MoveBufferArea(0, line, Console.BufferWidth, 1, Console.BufferWidth, line, ' ', Console.ForegroundColor, Console.BackgroundColor);
            Console.SetCursorPosition(cursor.Left, cursor.Top);
        }

        /// <summary>
        /// Создаёт файл с данными
        /// </summary>
        static void GenerateData()
        {
            string str = @"00:01 - 00:03 [Top, Red] Hello
00:01 - 00:03 [Bottom, Red] World
00:02 - 00:06 [Right, Green] Yes
00:04 - 00:07 [Left, Blue] No
00:07 - 00:15 Bill is a very motivated young man";

            File.WriteAllText(path.FullName, str);
        }
    }
}
